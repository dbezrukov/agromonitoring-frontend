define([
    'backbone',
    'models/equipment'
], function(Backbone, ItemModel) {
    return Backbone.Collection.extend({
        model: ItemModel,
        url: function() { 
        	return '/equipment';
        }
    });
});