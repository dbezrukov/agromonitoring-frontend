define([
    'backbone',
    'models/vegetation'
], function(Backbone, ItemModel) {
    return Backbone.Collection.extend({
        model: ItemModel,
        url: function() { 
        	return '/vegetation';
        }
    });
});