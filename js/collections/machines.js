define([
    'backbone',
    'models/machine'
], function(Backbone, ItemModel) {
    return Backbone.Collection.extend({
        model: ItemModel,
        url: function() { 
        	return '/machines';
        },

        fetchPositionUpdates: function() {

        	/*
        	var self = this;

        	self.fetch({
        		data: { page: 1 },
    			processData: true
				success: function() {
					// get latest timestamp
					self.lastTimestamp = timestamp
				}
	    	});
			*/
        },

        startMonitoring: function() {
        	var self = this;

        	self.timer = setInterval(function() {
        		self.fetchPositionUpdates();
        	}, 2000);

        	self.fetchPositionUpdates();
        },

        stopMonitoring: function() {
        	var self = this;

        	// started already
        	if (self.timer) {
        		clearInterval(self.timer);

        		// switch off all valid positions
        	}
        }

    });
});
