define([
    'backbone',
    'models/news'
], function(Backbone, ItemModel) {
    return Backbone.Collection.extend({
        model: ItemModel,
        url: function() { 
        	return '/news';
        }
    });
});