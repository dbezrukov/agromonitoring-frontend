define([
    'backbone',
    'models/seeding'
], function(Backbone, ItemModel) {
    return Backbone.Collection.extend({
        model: ItemModel,
        url: function() { 
        	return '/seedings';
        }
    });
});