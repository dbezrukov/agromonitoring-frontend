define([
    'backbone',
    'models/crop'
], function(Backbone, ItemModel) {
    return Backbone.Collection.extend({
        model: ItemModel,
        url: function() { 
        	return '/crops';
        }
    });
});