define([
    'backbone',
    'models/seeding_stats'
], function(Backbone, ItemModel) {
    return Backbone.Collection.extend({
        model: ItemModel,
        url: function() { 
        	return '/seedings/stats/' + app.account.get('addInfo').activeYear;
        }
    });
});