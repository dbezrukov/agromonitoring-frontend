define([
    'backbone',
    'models/field'
], function(Backbone, ItemModel) {
    return Backbone.Collection.extend({
        model: ItemModel,
        url: function() { 
        	return '/fields';
        }
    });
});