define([
    'jquery',
    'underscore',
    'marionette',
    'collections/news',
    'collections/seeding_stats',
    'widgets/map/map_widget',
    'views/news_widget',
    'views/funds_widget',
    'views/seeding_stats_widget',
    'tpl!templates/monitoring_pane.tmpl',
    'css!stylesheets/monitoring_pane_style',

], function($, _, Marionette, 
	News, SeedingStats, 
	MapWidget, NewsWidget, FundsWidget, SeedingStatsWidget, 
	template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'container monitoring-pane',

        ui: {
        	'mapWidgetWrapper' : '.map-widget-wrapper',
        	'statsWidgetWrapper' : '.seeding-stats-widget-wrapper',
        	'newsWidgetWrapper' : '.news-widget-wrapper',
        	'fundsWidgetWrapper' : '.funds-widget-wrapper'
        },

        events: {
        },

        modelEvents: {
        },

        onRender: function() {

        	var self = this;

        	self.initMap();
        	self.initStats();
        	self.initNews();
        	self.initFunds();
        },

        initMap: function() {
        	var self = this;

        	var ndviBoundary = { 
        		type: "MultiPolygon",
        		coordinates: [] 
        	}

        	app.fields.forEach(function(model) {
				var polygon = model.get('polygon');
				if (polygon.coordinates.length > 0) {
					ndviBoundary.coordinates.push(polygon.coordinates);
				}
			});

        	self.mapWidget = new MapWidget({
            	model: self.model,
            	ndviBoundary: ndviBoundary
        	});

        	self.ui.mapWidgetWrapper.append(self.mapWidget.render().el);

        	app.fields.forEach(function(model) {
				
				var geometry = model.get('polygon');
				if (geometry.coordinates.length > 0) {
					
					var lastVegetation = model.get('lastVegetation');

					var feature = {
						type: 'Feature',
						properties: {
							'Поле': '<a title="Открыть" href="#fields/' + model.get('_id') + '">' + model.get('name') + '</a>',
							'NVDI': lastVegetation ? lastVegetation.value : 'н/д'
						},
						geometry: geometry
					}

					self.mapWidget.geojsonLayer.addData(feature);
				}
			});
        },

        initStats: function() {
        	var self = this;

        	var stats = new SeedingStats();
        	stats.fetch({
		    	success: function() {
		    		var statsWidget = new SeedingStatsWidget({
            			collection: stats
        			});

	            	self.ui.statsWidgetWrapper.append(statsWidget.render().el);
		        }
			})
        },

        initNews: function() {
        	var self = this;

        	var news = new News();
        	news.fetch({
		    	success: function() {
		    		var newsWidget = new NewsWidget({
            			collection: news
        			});

	            	self.ui.newsWidgetWrapper.append(newsWidget.render().el);
		        }
			})
        },

        initFunds: function() {
        	var self = this;

    		var fundsWidget = new FundsWidget({
			});

        	self.ui.fundsWidgetWrapper.append(fundsWidget.render().el);
        },

        onShow: function() {  
            var self = this;
            
            self.mapWidget.fitBounds();
        },
    });

});
