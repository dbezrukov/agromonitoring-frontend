define([
    'jquery',
    'underscore',
    'marionette',
    'views/equipment_item',
    'tpl!templates/telematics_equipment_pane.tmpl',
    'css!stylesheets/telematics_equipment_pane_style',
    
], function($, _, Marionette, ItemView, template) {

    return Marionette.CompositeView.extend({
        template: template,
        itemView: ItemView,
        itemViewContainer: '.equipment',
        tagName: "div",
        className: "container equipment-pane",

        ui: {
        },

        collectionEvents: {
        },
        
        initialize: function(options) {
            var self = this;

            options || (options = {});
            self.options = options;
        },

        onRender: function() {
        	var self = this;

        	self.collection.fetch();
		}
    });
});