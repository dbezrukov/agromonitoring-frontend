define([
    'jquery',
    'underscore',
    'marionette',
    'tpl!templates/report_vegetation_pane.tmpl',
    'highcharts'

], function($, _, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: "container vegetationPane padding-top",
        
        ui: {
        	'widgetVegetation': '#widget-vegetation'
        },

        events: {
        },

        onRender: function() {
        	var self = this;

        	//self.setupChart();
		}, 

		onShow: function() {  
            var self = this;
            //self.ui.widgetVegetation.highcharts().reflow();
        },

        setupChart: function() {
        	var self = this;

        	self.ui.widgetVegetation.highcharts({
		    });
        }
    })

});
