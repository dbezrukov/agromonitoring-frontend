define([
    'jquery',
    'underscore',
    'marionette',
    'models/seeding_stats',
    'views/seeding_stats_item',
    'tpl!templates/seeding_stats_widget.tmpl',
    'css!stylesheets/seeding_stats_widget_style',
], function($, _, Marionette, ItemModel, ItemView, template) {

    return Marionette.CompositeView.extend({
        template: template,
        itemView: ItemView,
        itemViewContainer: '.stats',
        tagName: "div",
        className: "widget seeding-stats-widget",

        ui: {
        },

        events: {
        },

        collectionEvents: {
        },
        
        initialize: function(options) {
            var self = this;

            options || (options = {});
            self.options = options;
        },

        onRender: function() {
        	var self = this;
		}
    });
});