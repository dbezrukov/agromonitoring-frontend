define([
    'jquery',
    'underscore',
    'marionette',
    'views/seeding_stats_item_fields_item',
    'tpl!templates/seeding_stats_item_fields.tmpl',
    'css!stylesheets/seeding_stats_item_fields_style'

], function($, _, Marionette, ItemView, template) {

    return Marionette.CompositeView.extend({
        template: template,
        itemView: ItemView,
        itemViewContainer: '.stats-fields',
        tagName: "table",
        className: "table table-condensed seeding-stats-item-fields",

        ui: {
        },

        events: {
        },

        collectionEvents: {
        },
        
        initialize: function(options) {
            var self = this;

            options || (options = {});
            self.options = options;
        },

        onRender: function() {
        	var self = this;
		}
    });
});