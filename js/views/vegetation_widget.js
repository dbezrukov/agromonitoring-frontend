define([
    'jquery',
    'underscore',
    'marionette',
    'models/vegetation',
    'css!stylesheets/chart_widget_style',
    'highcharts'
], function($, _, Marionette, Vegetation) {

    return Marionette.ItemView.extend({
        template: _.template(''),
        tagName: "div",
        className: "chart-widget",

        ui: {
        },

        events: {
        },

        collectionEvents: {
        },
        
        initialize: function(options) {
            var self = this;

            options || (options = {});
            self.options = options;

            Highcharts.setOptions({
				lang: {
					shortMonths: [ "Янв" , "Фев" , "Мар" , "Апр" , "Май" , "Июн" , "Июл" , "Авг" , "Сен" , "Окт" , "Нов" , "Дек"],
					weekdays: ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье']
				}
			});
        },

        onRender: function() {
        	var self = this;

        	self.initChart();

        	self.fetchSeries(function(err, series) {
        		if (err) {
        			console.log(err);
        			return;
        		}

        		$(self.el).highcharts().series[0].setData(series.data);
        	})
		}, 

		fetchSeries: function(callback) {  
            var self = this;

            var annualVegetation = app.vegetations.findWhere({
				field: self.model.get('_id'),
				year: app.account.get('addInfo').activeYear
			});

			if (!annualVegetation) {
				annualVegetation = new Vegetation({
					field: self.model.get('_id'),
					year: app.account.get('addInfo').activeYear
				})

				app.vegetations.push(annualVegetation);
			}

            annualVegetation.fetch({
    			success: function() {
    				var data = annualVegetation.get('data');

    				if (!data) {
    					return callback('vegetation fetch error');
    				}
    				
					var series = {
	        			name: 'Поле ' + self.model.get('name'),
						color: 'rgb(32,104,0)',
	        			data: data
	        		}

        			return callback(null, series);
        		},
        		error: function() {
        			return callback('vegetation fetch error');
        		}
        	})
        },

        initChart: function(series) {  
            var self = this;

            $(self.el).highcharts({
		        chart: {
		            type: 'spline'
		        },
		        title: {
		            text: ''
		        },
		        credits: {
		        	enabled: false
		        },
		        xAxis: {
		            type: 'datetime',
		            dateTimeLabelFormats: {
		                month: '%b',
		                year: '%b'
		            },
		            title: {
		                text: ''
		            },
		            crosshair: true,
					events: {
						setExtremes: window.syncExtremes
					},
		        },
		        yAxis: {
		            title: {
		                text: ''
		            },
		            maxPadding: 0.1,
		            minPadding: 0.1
		        },

		        tooltip: {
				   	useHTML: true,
				   	shared: false,
				   	borderRadius: 0,
				   	borderWidth: 0,
				   	borderWidth: 0,
				   	shadow: false,
				   	enabled: true,
				   	backgroundColor: 'none',
				   	formatter: function() {
				   		var ts = new Date(this.point.x).getTime();
				      	return '<span>' + Highcharts.dateFormat('%e %b %Y', ts) + '</span>';
				   	},
				   	positioner: function (labelWidth, labelHeight, point) {
		                var tooltipX, tooltipY;
		                tooltipX = point.plotX + 60;
		                tooltipY = 50;
		                return {
		                    x: tooltipX,
		                    y: tooltipY
		                };
		            }
				},

		        plotOptions: {
		            spline: {
		                marker: {
		                    enabled: false
		                }
		            }
		        },

		        series: [{
		        	name: 'Поле ' + self.model.get('name'),
					color: 'rgb(32,104,0)',
        			data: []
		        }]
		    });
        },

		fitBounds: function() {  
            var self = this;
            
            $(self.el).highcharts().reflow();
        },
    });
});
