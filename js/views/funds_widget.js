define([
    'jquery',
    'underscore',
    'marionette',
    'tpl!templates/funds_widget.tmpl',
    'css!stylesheets/funds_widget_style',
], function($, _, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: "div",
        className: "widget funds-widget",

        ui: {
        },

        events: {
        },

        initialize: function(options) {
            var self = this;

        },

        onRender: function() {
        	var self = this;
		}
    });
});