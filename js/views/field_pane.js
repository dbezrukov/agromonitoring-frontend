define([
	'underscore',
    'marionette',
    'views/field_overall_pane',
    'tpl!templates/field_pane.tmpl',
    'css!stylesheets/field_pane_style',
    
], function(_, Marionette, 
    FieldOverall, template) {

	return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'container field-pane padding-top',

        ui: {
        	'fieldOverall' : '#field-overall-tab'
        },

        events: {
        },

        modelEvents: {
        },

        templateHelpers: {
        },

		onRender: function() {

        	var self = this;

        	self.initOverall();

        	$(self.el).find('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  				self.fieldOverall.onShow.call(self.fieldOverall);
			});
        },

		initOverall: function() {
        	var self = this;

        	self.fieldOverall = new FieldOverall({
            	model: self.model
        	});

        	self.ui.fieldOverall.append(self.fieldOverall.render().el);
		},

        onShow: function() {  
        	var self = this;

        	self.fieldOverall.onShow.call(self.fieldOverall);
        }
    });
});
