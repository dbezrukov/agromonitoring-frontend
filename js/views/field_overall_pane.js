define([
	'underscore',
    'marionette',
    'views/field_overall_monitoring_pane',
    'tpl!templates/field_overall_pane.tmpl'
    
], function(_, Marionette,
	MonitoringPane, 
	template) {

	return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'field-overall-pane',

        ui: {
        	'tabContent': '.tab-content'
        },

        events: {
        },

        modelEvents: {
        },

        templateHelpers: {
        },

		onRender: function() {
        	var self = this;

			self.initMonitoring();
        	//self.initTelematics();
        	//self.initAgroOperations();
        	//self.initImages();
        },

        initMonitoring: function() {
        	var self = this;

        	self.monitoringPane = new MonitoringPane({
            	model: self.model
        	});

        	self.ui.tabContent.append(self.monitoringPane.render().el);
        },

        initTelematics: function() {
        	var self = this;
        },

        initAgroOperations: function() {
        	var self = this;
        },

        initImages: function() {
        	var self = this;
        },

        onShow: function() {  
            var self = this;

			self.monitoringPane.onShow.call(self.monitoringPane);
        }
    });
});
