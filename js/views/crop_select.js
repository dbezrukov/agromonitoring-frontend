define([
	'underscore',
    'marionette',
    'tpl!templates/crop_select.tmpl',
    'css!stylesheets/crop_select_style',
    'css!bower_components/Ionicons/css/ionicons.min.css',
    'bootstrap-multiselect'
], function(_, Marionette, template) {

    function multiselectData(entities) {
    	var data = [];

    	data.push({ label: 'н/д', value: '' });

    	entities.forEach(function(entity) {
        	data.push({ label: entity.get('name'), value: entity.get('_id') });
    	});

    	return data;
	}

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'crop-select',

        ui: {
        	'select' : '.multiselect'
        },

        events: {
        },

        modelEvents: {
        	'change:cropId': 'onCropIdChanged'
        },

        initialize: function(options) {
            var self = this;

            options || (options = {});
            self.options = options;
        },

        onRender: function() {
        	var self = this;

    		self.ui.select.multiselect({
	        	nonSelectedText: '',
        		onChange: function(option, checked) {

        			if (checked) {

        				var cropId = option.val();

        				self.model.save({
        					'cropId': cropId
        				}, 
        				self.model.saveOptions);
        			}
            	}
            });	

			self.ui.select.multiselect('dataprovider', multiselectData(app.crops));

			self.onCropIdChanged();
			self.ui.select.multiselect('rebuild');

			if (self.options.disabled) {
				self.ui.select.parent().find('button').css('pointer-events', 'none');
				self.ui.select.parent().find('.caret').css('display', 'none');
			}
        },

        onCropIdChanged: function() {
        	var self = this;

        	var cropId = self.model.get('cropId');

        	if (cropId.length > 0) {
	        	self.ui.select.multiselect('select', cropId, false);

	        	var crop = app.crops.get(cropId);
	        	self.ui.select.parent().find('button').css('backgroundColor', crop.get('color'));
	        } else {
	        	self.ui.select.parent().find('button').css('backgroundColor', 'transparent');
	        }
        }
    });
});
