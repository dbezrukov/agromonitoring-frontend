define([
    'jquery',
    'underscore',
    'marionette',
    'models/field',
    'views/field_item',
    'tpl!templates/fields_pane.tmpl',
    'css!stylesheets/fields_pane_style',
], function($, _, 
    Marionette, 
    ItemModel, 
	ItemView,
	template) {

    return Marionette.CompositeView.extend({
        template: template,
        itemView: ItemView,
        itemViewContainer: '.fields',
        tagName: "div",
        className: "container fields-pane padding-top",

        ui: {
        },

        collectionEvents: {
        },
        
        initialize: function(options) {
            var self = this;

            options || (options = {});
            self.options = options;
        },

        onRender: function() {
        	var self = this;

        	self.collection.fetch();
		}
    });
});