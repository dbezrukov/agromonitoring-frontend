define([
	'underscore',
    'marionette',
    'models/seeding',
    'views/crop_select',
    'ndvi',
    'tpl!templates/field_item.tmpl',
    'css!stylesheets/field_item_style',
    'css!bower_components/Ionicons/css/ionicons.min.css',
], function(_, Marionette, Seeding, CropSelect, Ndvi, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'tr',
        className: 'field-item',

        ui: {
        	'fieldCrop' : '#field-crop',
        	'fieldSow' : '#field-sow',
        	'fieldHarvest' : '#field-harvest',
        	'fieldNdvi' : '#field-ndvi'
        },

        events: {
        },

        modelEvents: {
        },

        onRender: function() {
        	var self = this;

			var seeding = app.seedings.findWhere({
				field: self.model.get('_id'),
				year: app.account.get('addInfo').activeYear
			});

			if (!seeding) {
				console.log('seeding not found');
				return;
			}

    		var cropSelect = new CropSelect({
            	model: seeding,
            	disabled: true
        	});

        	self.ui.fieldCrop.append(cropSelect.render().el);

        	self.ui.fieldSow.text(seeding.get('dateSow') || '—');
        	self.ui.fieldHarvest.text(seeding.get('dateHarvest')  || '—');

			var lastVegetation = self.model.get('lastVegetation');

			if (lastVegetation) {
				self.ui.fieldNdvi.text(lastVegetation.value);
				self.ui.fieldNdvi.css('color', Ndvi.ndviColor(lastVegetation.value));
			} else {
				self.ui.fieldNdvi.text('—');
			}
        }
    });
});
