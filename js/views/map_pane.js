define([
    'jquery',
    'underscore',
    'marionette',
    'widgets/map/map_widget',
    'tpl!templates/map_pane.tmpl',
    'css!stylesheets/map_pane_style',

], function($, _, Marionette, MapWidget, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'map-pane',

        ui: {
        	'mapWidgetWrapper': '.map-widget-wrapper',
        },

        events: {
        },

        modelEvents: {
        },

        onRender: function() {

        	var self = this;

        	self.initMap();
        },

        initMap: function() {
        	var self = this;

        	var ndviBoundary = { 
        		type: "MultiPolygon",
        		coordinates: [] 
        	}

        	app.fields.forEach(function(model) {
				var polygon = model.get('polygon');
				if (polygon.coordinates.length > 0) {
					ndviBoundary.coordinates.push(polygon.coordinates);
				}
			});
			
        	self.mapWidget = new MapWidget({
            	model: self.model,
            	ndviBoundary: ndviBoundary
        	});

        	self.ui.mapWidgetWrapper.append(self.mapWidget.render().el);

        	app.fields.forEach(function(model) {
        		
				var geometry = model.get('polygon');
				if (geometry.coordinates.length > 0) {
					
					var lastVegetation = model.get('lastVegetation');

					var feature = {
						type: 'Feature',
						properties: {
							'Поле': '<a title="Открыть" href="#fields/' + model.get('_id') + '">' + model.get('name') + '</a>',
							'NVDI': lastVegetation ? lastVegetation.value : 'н/д'
						},
						geometry: geometry
					}

					self.mapWidget.geojsonLayer.addData(feature);
				}
			});
		},

        onShow: function() {  
            var self = this;
			self.mapWidget.fitBounds();
        },
    });
});
