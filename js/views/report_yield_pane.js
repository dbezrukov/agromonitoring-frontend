define([
    'jquery',
    'underscore',
    'marionette',
    'tpl!templates/report_yield_pane.tmpl',
    'highcharts'

], function($, _, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'container yeldPane padding-top',
        
        ui: {
        	'widgetYield': '#widget-yield'
        },

        events: {
        },

        onRender: function() {
        	var self = this;

        	//self.setupChart();
		}, 

		onShow: function() {  
            var self = this;
            //self.ui.widgetYield.highcharts().reflow();
        },

        setupChart: function() {
        	var self = this;

        	self.ui.widgetYield.highcharts({
		    });
        }
    })

});
