define([
	'underscore',
    'marionette',
    'tpl!templates/equipment_item.tmpl',
    'css!stylesheets/equipment_item_style'

], function(_, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'tr',
        className: 'equipment-item',

        ui: {
        },

        events: {
        },

        modelEvents: {
        },

        onRender: function() {
        	var self = this;
        }
    });
});
