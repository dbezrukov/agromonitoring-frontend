define([
	'underscore',
    'marionette',
    'ndvi',
    'tpl!templates/seeding_stats_item_fields_item.tmpl',

], function(_, Marionette, Ndvi, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'tr',

        ui: {
        	'fieldNdvi' : '#field-ndvi'
        },

        modelEvents: {
        	'change': 'render'
        },

        onRender: function() {
        	var self = this;

        	self.model.fetch({
		    	success: function() {
		    		var lastVegetation = self.model.get('lastVegetation');

					if (lastVegetation) {
						self.ui.fieldNdvi.text(lastVegetation.value);
        				self.ui.fieldNdvi.css('color', Ndvi.ndviColor(lastVegetation.value));
        				self.ui.fieldNdvi.css('border-left-color', Ndvi.ndviColor(lastVegetation.value));
					} else {
						self.ui.fieldNdvi.text('—');
					}
				}
			})
		}
    });
});
