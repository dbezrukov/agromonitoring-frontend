define([
	'underscore',
    'marionette',
    'views/crop_select',
    'tpl!templates/seeding_info.tmpl',
    'css!stylesheets/seeding_info_style',
    'x-editable',
    'css!bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css'

], function(_, Marionette, CropSelect, template) {

	return Marionette.ItemView.extend({
        template: template,
        tagName: 'tbody',
        className: 'seeding-info',

        ui: {
        	'fieldCrop' : '#field-crop',
        	'fieldVariety' : '#field-variety',
	      	'fieldYeld' : '#field-yeld',
	      	'fieldTillage' : '#field-tillage',
	      	'fieldSow' : '#field-sow',
	      	'fieldHarvest' : '#field-harvest'
        },

        modelEvents: {
        	'change': 'render'
        },

        onRender: function() {

        	var self = this;

        	var cropSelect = new CropSelect({
	  			model: self.model
	  		});

			self.ui.fieldCrop.append(cropSelect.render().el);

			self.ui.fieldVariety.editable({
                type: 'text',
    			mode: 'inline',
    			showbuttons: false,
    			emptytext: '—',
    			success: function(response, newValue) {
        			self.model.save({
        				'variety': newValue
        			}, 
        			self.model.saveOptions);
    			}
            });

            self.ui.fieldYeld.editable({
                type: 'text',
    			mode: 'inline',
    			showbuttons: false,
    			emptytext: '—',
    			display: function(value) {
      				$(this).text(value + ' ц/га');
    			},
    			success: function(response, newValue) {
        			self.model.save({
        				'yield': newValue
        			}, 
        			self.model.saveOptions);
    			}
            });

            self.ui.fieldTillage.editable({
                type: 'text',
    			mode: 'inline',
    			showbuttons: false,
    			emptytext: '—',
    			success: function(response, newValue) {
        			self.model.save({
        				'tillage': newValue
        			}, 
        			self.model.saveOptions);
    			}
            });

            self.ui.fieldSow.editable({
                type: 'text',
    			mode: 'inline',
    			showbuttons: false,
    			emptytext: '—',
    			success: function(response, newValue) {
        			self.model.save({
        				'dateSow': newValue
        			}, 
        			self.model.saveOptions);
    			}
            });

            self.ui.fieldHarvest.editable({
                type: 'text',
    			mode: 'inline',
    			showbuttons: false,
    			emptytext: '—',
    			success: function(response, newValue) {
        			self.model.save({
        				'dateHarvest': newValue
        			}, 
        			self.model.saveOptions);
    			}
            });
		}
	});
});
