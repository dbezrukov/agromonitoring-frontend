define([
    'jquery',
    'underscore',
    'marionette',
    'tpl!templates/company_pane.tmpl',

], function($, _, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'container companyPane padding-top',
        
        ui: {
        },

        events: {
        }
    })

});
