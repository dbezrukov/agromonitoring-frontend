define([
	'underscore',
    'marionette',
    'collections/fields',
    'views/seeding_stats_item_fields', 
    'tpl!templates/seeding_stats_item.tmpl',
    'css!stylesheets/seeding_stats_item_style'

], function(_, Marionette, Fields, ItemFields, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'tbody',
        className: 'seeding-stats-item',

        ui: {
        	'statsCropCollapse' : '.stats-crop-collapse'
        },

        events: {
        },

        modelEvents: {
        },

        templateHelpers: {
        	getCropName: function() {
        		var crop = app.crops.get(this.cropId);
	        	return crop.get('name');
        	}
        },

        onRender: function() {
        	var self = this;

			var ids = _.map(self.model.get('fields'), function(val) {
    			return { _id: val };
			});

			var fields = new Fields(ids);

			var itemFields = new ItemFields({
				collection: fields
			});

			$(self.ui.statsCropCollapse).append(itemFields.render().el);
        }
    });
});
