define([
	'underscore',
    'marionette',
    'models/seeding',
    'views/crop_select', 
    'tpl!templates/field_rotation_item.tmpl',
    'css!stylesheets/field_rotation_item_style',
    'css!bower_components/Ionicons/css/ionicons.min.css'

], function(_, Marionette, Seeding, CropSelect, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'tr',
        className: 'field-rotation-item',

        ui: {
        	'cropCell' : '.crop-cell'
        },

        events: {
        },

        modelEvents: {
        },

        onRender: function() {
        	var self = this;

        	self.ui.cropCell.each(function(n, el) {
				var year = $(el).attr('year');

				var seeding = app.seedings.findWhere({
					field: self.model.get('_id'),
					year: year
				});

				if (!seeding) {
					console.log('seeding not found');
					return;
				}

	    		var cropSelect = new CropSelect({
                	model: seeding
            	});

            	$(el).append(cropSelect.render().el);
			})
        },
    });
});
