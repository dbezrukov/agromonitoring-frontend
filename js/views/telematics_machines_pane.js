define([
    'jquery',
    'underscore',
    'marionette',
    'views/machine_item',
    'tpl!templates/telematics_machines_pane.tmpl',
    'css!stylesheets/telematics_machines_pane_style',
], function($, _, Marionette, ItemView, template) {

    return Marionette.CompositeView.extend({
        template: template,
        itemView: ItemView,
        itemViewContainer: '.machines',
        tagName: "div",
        className: "container machines-pane",

        ui: {
        },

        collectionEvents: {
        },
        
        initialize: function(options) {
            var self = this;

            options || (options = {});
            self.options = options;
        },

        onRender: function() {
        	var self = this;

        	self.collection.fetch();
		}
    });
});