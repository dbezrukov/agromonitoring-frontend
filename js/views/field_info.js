define([
	'underscore',
    'marionette',
    'models/seeding',
    'views/seeding_info', 
    'tpl!templates/field_info.tmpl',
    'css!stylesheets/field_info_style',
    'x-editable',
    'css!bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css',

], function(_, Marionette, Seeding, SeedingInfo, template) {

	return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'widget field-info',

        ui: {
        	'fieldName': '#field-name',
        	'fieldPlace': '#field-place',
        	'fieldAreaCultivated': '#field-area-cultivated',
        	'fieldAreaOfficial': '#field-area-official',
        	'seedingInfo' : '#seeding-info'
        },

        modelEvents: {
        	'change': 'render'
        },

        onRender: function() {

        	var self = this;

        	self.ui.fieldName.editable({
                type: 'text',
    			mode: 'inline',
    			showbuttons: false,
    			emptytext: '—',
    			success: function(response, newValue) {
        			self.model.save({
        				'name': newValue
        			}, 
        			self.model.saveOptions);
    			}
            });

            self.ui.fieldPlace.editable({
                type: 'text',
    			mode: 'inline',
    			showbuttons: false,
    			emptytext: '—',
    			success: function(response, newValue) {
        			self.model.save({
        				'place': newValue
        			}, 
        			self.model.saveOptions);
    			}
            });

            self.ui.fieldAreaCultivated.editable({
                type: 'text',
    			mode: 'inline',
    			showbuttons: false,
    			emptytext: '—',
    			display: function(value) {
      				$(this).text(value + ' га');
    			},
    			success: function(response, newValue) {
    				self.model.save({
        				'area': {
        					cultivated: newValue,
        					official: self.model.get('area').official
        				}
        			}, 
        			self.model.saveOptions);
    			}
            });

            self.ui.fieldAreaOfficial.editable({
                type: 'text',
    			mode: 'inline',
    			showbuttons: false,
    			emptytext: '—',
    			display: function(value) {
      				$(this).text(value + ' га');
    			},
    			success: function(response, newValue) {
        			self.model.save({
        				'area': {
        					cultivated: self.model.get('area').cultivated,
        					official: newValue
        				}
        			}, 
        			self.model.saveOptions);
    			}
            });

            self.initSeedingInfo();
        }, 

        initSeedingInfo: function() {
        	var self = this;

			var seeding = app.seedings.findWhere({
				field: self.model.get('_id'),
				year: app.account.get('addInfo').activeYear
			});

			if (!seeding) {
				console.log('seeding not found');
				return;
			}

    		var seedingInfo = new SeedingInfo({
            	model: seeding
        	});

			self.ui.seedingInfo.append(seedingInfo.render().el);
        },
    });
});
