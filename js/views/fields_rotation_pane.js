define([
    'jquery',
    'underscore',
    'marionette',
    'views/field_rotation_item',
    'tpl!templates/fields_rotation_pane.tmpl',
    'css!stylesheets/fields_rotation_pane_style'

], function($, _, Marionette, 
	ItemView,
	template) {

    return Marionette.CompositeView.extend({
        template: template,
        itemView: ItemView,
        itemViewContainer: '.fields',
        tagName: "div",
        className: "container fields-rotation-pane padding-top",

        ui: {
        },

        collectionEvents: {
        },
        
        initialize: function(options) {
            var self = this;

            options || (options = {});
            self.options = options;
        },

        onRender: function() {
        	var self = this;

        	self.collection.fetch();
		}
    });
});





		