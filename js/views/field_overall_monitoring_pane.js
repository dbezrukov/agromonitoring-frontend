define([
	'underscore',
    'marionette',
    'views/field_info',
    'views/ndvi_widget',
    'widgets/map/map_widget',
    'views/vegetation_widget',
    'views/temperature_widget',
    'views/precipitation_widget',
    'views/humidity_widget',
    'tpl!templates/field_overall_monitoring_pane.tmpl',
    'css!bower_components/Ionicons/css/ionicons.min.css',
    'highcharts',
    'x-editable',
    'css!bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css',
    
], function(_, Marionette, FieldInfo, 
	NdviWidget, MapWidget, VegetationWidget, TemperatureWidget, PrecipitationWidget, HumidityWidget, 
	template) {

	return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'tab-pane active',
        id: 'field-monitoring-tab',

        ui: {
        	'fieldNdviDate': '#field-ndvi-date',
			'fieldInfoWrapper' : '.field-info-wrapper',
        	'ndviWidgetWrapper': '.ndvi-widget-wrapper',
        	'mapWidgetWrapper': '.map-widget-wrapper',
        	'vegetationWidgetWrapper': '.vegetation-widget-wrapper',
			'temperatureWidgetWrapper': '.temperature-widget-wrapper',
			'precipitationWidgetWrapper': '.precipitation-widget-wrapper',
			'humidityWidgetWrapper': '.humidity-widget-wrapper'
        },

        events: {
        	'click a.open': 'fitMapBounds',
            'click a.edit': 'editField',
            'click a.delete': 'deleteField'
        },

        modelEvents: {
        },

        templateHelpers: {
        },

		deleteField: function(){

			var confirmation = confirm('Удалить это поле?');

		    if (confirmation !== true) {
		        return;
		    }

            this.model.destroy();

            app.route.navigate('#fields', {trigger: true})
        },

        onRender: function() {

        	var self = this;

        	self.initFieldInfo();
        	self.initMap();

        	self.initLinkedCharts();
        	self.initVegetation();
        	self.initTemperature();
        	self.initPrecipitation();
        	self.initHumidity();
        	self.startLinkedCharts();

        	self.initNdvi();
        },

        initFieldInfo: function() {
        	var self = this;

        	var fieldInfo = new FieldInfo({
            	model: self.model
        	});

        	self.ui.fieldInfoWrapper.append(fieldInfo.render().el);
		},

		initMap: function() {
        	var self = this;

			var polygon = self.model.get('polygon');
			
        	self.mapWidget = new MapWidget({
        		ndviBoundary: polygon
        	});

        	self.ui.mapWidgetWrapper.append(self.mapWidget.render().el);

			// adding non-empty polygon
			if (polygon.coordinates.length > 0) {
				polygon.properties = {
					'Поле': self.model.get('name')
				}
				self.mapWidget.geojsonLayer.addData(polygon);
			}
		},

        initNdvi: function() {
        	var self = this;

        	self.ndviWidget = new NdviWidget({
            	model: self.model
        	});

        	self.ui.ndviWidgetWrapper.append(self.ndviWidget.render().el);
		},

        initVegetation: function() {
        	var self = this;

        	self.vegetationWidget = new VegetationWidget({
            	model: self.model
        	});

        	self.ui.vegetationWidgetWrapper.append(self.vegetationWidget.render().el);
        },

        initTemperature: function() {
        	var self = this;

        	self.temperatureWidget = new TemperatureWidget({
            	model: self.model
        	});

        	self.ui.temperatureWidgetWrapper.append(self.temperatureWidget.render().el);
        },

        initPrecipitation: function() {
        	var self = this;

        	self.precipitationWidget = new PrecipitationWidget({
            	model: self.model
        	});

        	self.ui.precipitationWidgetWrapper.append(self.precipitationWidget.render().el);
        },

        initHumidity: function() {
        	var self = this;

        	self.humidityWidget = new HumidityWidget({
            	model: self.model
        	});

        	self.ui.humidityWidgetWrapper.append(self.humidityWidget.render().el);
        },

        onShow: function() {  
            var self = this;

			self.mapWidget.fitBounds();
			self.vegetationWidget.fitBounds();
			self.temperatureWidget.fitBounds();
			self.precipitationWidget.fitBounds();
			self.humidityWidget.fitBounds();
        },

		fitMapBounds: function() {
        	var self = this;

        	self.mapWidget.fitBounds();
        },

        initLinkedCharts: function() {

        	/**
		     * Override the reset function, we don't need to hide the tooltips and crosshairs.
		     */
		    Highcharts.Pointer.prototype.reset = function () {
		        return undefined;
		    };

		    /**
		     * Synchronize zooming through the setExtremes event handler.
		     */
		    window.syncExtremes = function(e) {
		        var thisChart = this.chart;

		        if (e.trigger !== 'syncExtremes') { // Prevent feedback loop
		            Highcharts.each(Highcharts.charts, function(chart) {
		                if (chart !== thisChart) {
		                    if (chart.xAxis[0].setExtremes) { // It is null while updating
		                        chart.xAxis[0].setExtremes(e.min, e.max, undefined, false, {
		                            trigger: 'syncExtremes'
		                        });
		                    }
		                }
		            });
		        }
		    }
        }, 

        startLinkedCharts: function() {

        	var self = this;

        	var charts = $(self.el).find('.highcharts-container');

    		charts.bind('mousemove touchmove touchstart', function (e) {

		        var chart,
		            point,
		            i;
		            
		        // Find coordinates within the chart. In case charts are side by side,
		        // use the chart we are hovering.
		        for (i = 0; i < Highcharts.charts.length; i = i + 1) {
		            chart = Highcharts.charts[i];
		            e = chart.pointer.normalize(e); 
		            if (e.chartX > chart.xAxis[0].pos && e.chartX < chart.xAxis[0].pos + chart.xAxis[0].len) {
		            	break;
		            }
				}
		        
		        for (i = 0; i < Highcharts.charts.length; i = i + 1) {
		            chart = Highcharts.charts[i];

		            if ($(chart.renderTo).hasClass('ndvi-chart')) {
						// skip linking ndvi chart
		            	return;
		            }

		            point = chart.series[0].searchPoint(e, true); // Get the hovered point
		            if (point) {
		                //point.onMouseOver(); // Show the hover marker
		                //chart.tooltip.refresh(point); // Show the tooltip
		                chart.xAxis[0].drawCrosshair(e, point); // Show the crosshair
		            }
		        }
		    })

			charts.bind('mouseleave', function(e) {

				var chart,
		            point,
		            i;
		            
		        // Find coordinates within the chart. In case charts are side by side,
		        // use the chart we are hovering.
		        for (i = 0; i < Highcharts.charts.length; i = i + 1) {
		            chart = Highcharts.charts[i];
		            e = chart.pointer.normalize(e); 
		            if (e.chartX > chart.xAxis[0].pos && e.chartX < chart.xAxis[0].pos + chart.xAxis[0].len) {
		            	break;
		            }
				}
		        for (i = 0; i < Highcharts.charts.length; i = i + 1) {
		            chart = Highcharts.charts[i];
		            point = chart.series[0].searchPoint(e, true); // Get the hovered point
		            if (point) {
		                point.onMouseOut(); 
			      		chart.tooltip.hide(point);
			      		chart.xAxis[0].hideCrosshair(); 
		            }
		        }
			});
        }
    });
});
