define([
    'jquery',
    'underscore',
    'marionette',
    'models/news',
    'views/news_item',
    'tpl!templates/news_widget.tmpl',
    'css!stylesheets/news_widget_style',
], function($, _, Marionette, ItemModel, ItemView, template) {

    return Marionette.CompositeView.extend({
        template: template,
        itemView: ItemView,
        itemViewContainer: '.news',
        tagName: "div",
        className: "widget news-widget",

        ui: {
        },

        events: {
        },

        collectionEvents: {
        },
        
        initialize: function(options) {
            var self = this;

            options || (options = {});
            self.options = options;
        },

        onRender: function() {
        	var self = this;
		}
    });
});