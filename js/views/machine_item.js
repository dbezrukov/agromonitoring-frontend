define([
	'underscore',
    'marionette',
    'tpl!templates/machine_item.tmpl',
    'css!stylesheets/machine_item_style'

], function(_, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'tr',
        className: 'machine-item',

        ui: {
        },

        events: {
        },

        modelEvents: {
        },

        onRender: function() {
        	var self = this;
        }
    });
});
