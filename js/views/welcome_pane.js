define([
    'jquery',
    'underscore',
    'marionette',
    'tpl!templates/welcome_pane.tmpl',
    'css!stylesheets/welcome_pane_style',

], function($, _, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'div',
        className: 'padding-top',
        
        ui: {
        },

        evets: {
        	
        }       
    })
});
