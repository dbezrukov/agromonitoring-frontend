define([
	'underscore',
    'marionette',
    'tpl!templates/news_item.tmpl',
    'css!stylesheets/news_item_style'

], function(_, Marionette, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: 'li',
        className: 'news-item',

        ui: {
        },

        events: {
        },

        modelEvents: {
        },

        onRender: function() {
        	var self = this;
        }
    });
});
