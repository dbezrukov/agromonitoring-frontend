define([
    'jquery',
    'underscore',
    'marionette',
    'ndvi',
    'tpl!templates/ndvi_widget.tmpl',
    'css!stylesheets/chart_widget_style',
    'css!stylesheets/ndvi_widget_style',
], function($, _, Marionette, Ndvi, template) {

    return Marionette.ItemView.extend({
        template: template,
        tagName: "div",
        className: "widget ndvi-widget",

        ui: {
        	'ndviDate': '.ndvi-date',
        	'ndviValue': '.ndvi-value'
        },

        events: {
        },

        collectionEvents: {
        },
        
        initialize: function(options) {
            var self = this;

            options || (options = {});
            self.options = options;
        },

        onRender: function() {
        	var self = this;

			var lastVegetation = self.model.get('lastVegetation');
			if (lastVegetation) {
				var date = 'Обновлено ' + (new Date(lastVegetation.date).toLocaleDateString());
				self.ui.ndviDate.text(date);

				self.ui.ndviValue.text(lastVegetation.value);
				self.ui.ndviValue.css('color', Ndvi.ndviColor(lastVegetation.value));
			} else {
				self.ui.ndviDate.text('—');
				self.ui.ndviValue.text('—');
			}
		}
    });
});