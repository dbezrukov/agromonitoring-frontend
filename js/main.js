require.config({
    waitSeconds: 30,
    baseUrl: './js/',

    paths: {
        'jquery': 'bower_components/jquery/jquery.min',
        'bootstrap': 'bower_components/bootstrap/dist/js/bootstrap.min',
        'bootstrap-multiselect': 'bower_components/bootstrap-multiselect/dist/js/bootstrap-multiselect',
        'underscore': 'bower_components/underscore/underscore-min',
        'backbone': 'bower_components/backbone/backbone-min',
        'backbone.wreqr': 'bower_components/backbone.wreqr/lib/amd/backbone.wreqr.min',
        'backbone.babysitter': 'bower_components/backbone.babysitter/lib/amd/backbone.babysitter.min',
        'marionette': 'bower_components/marionette/lib/core/amd/backbone.marionette.min',
        'tpl': 'bower_components/requirejs-tpl/tpl',
        'barcode': 'vendors/barcode',
        'ndvi': 'vendors/ndvi',
        'highcharts': 'bower_components/highcharts/highcharts',
        'x-editable': 'bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min',
        'sprintfjs': 'bower_components/sprintf/dist/sprintf.min',
        'papaparse': 'bower_components/papaparse/papaparse.min',
    },
    shim: {
        'jquery': {
            exports: '$'
        },
        'highcharts': {
            deps: ['jquery'],
        },
        'bootstrap': {
            deps: ['jquery'],
        },
        'bootstrap-multiselect': {
            deps: ['jquery', 'bootstrap'],
        },
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'marionette': {
            deps: ['backbone', 'underscore', 'jquery'],
            exports: 'Marionette'
        },
        'underscore': {
            exports: '_'
        },
        'x-editable' : {
        	deps: ['bootstrap']
        }
    },
    map: {
        '*': {
            'css': 'bower_components/require-css/css.min'
        }
    }
});

require([

    'jquery', 
    'app',
    'models/account',
    'models/company',
    'collections/fields',
    'collections/crops',
    'collections/seedings',
    'collections/vegetations',
    'css!bower_components/bootstrap/dist/css/bootstrap.min.css',
    'css!stylesheets/common_style',

 ], function($, app, Account, Company, Fields, Crops, Seedings, Vegetations) {

    $(function() {

        app.account = new Account();
		app.account.fetch({
			success: setupActiveYearAndGroup
		});

	    function setupActiveYearAndGroup() {
	    	var addInfo = app.account.get('addInfo') || {};

	    	if (addInfo.activeYear && addInfo.activeGroup) {
	    		console.log('year and group ok');
	    		return setupCompanyAndFields();
	    	}

	    	addInfo.activeYear = addInfo.activeYear || 2017;

			if (!addInfo.activeGroup) {
				var groups = app.account.get('groups').filter(function(group) { 
					return (group.name !== 'admins' && group.name !== 'moderators');
            	});

            	// check whether a user has an active group
            	if (groups.length == 0) {
            		alert('Не найдена организация для данного пользователя');
            		return;
            	}

            	addInfo.activeGroup = groups[0]._id;

            	var options = {
        			type: 'POST',
        			wait: true,
        			url: '/users/' + app.account.get('id'),
        			success: function() {
        				setupCompanyAndFields();
    				},
    				error: function() { 
    					alert('Не удается сохранить данные');
    				}
        		}

        		app.account.save({
        			addInfo: addInfo
        		}, options);
			}
	    }

	    function setupCompanyAndFields() {
	    	var activeGroup = app.account.get('addInfo').activeGroup;
	    	var activeYear = app.account.get('addInfo').activeYear;

	    	console.log('activeGroup: ' + activeGroup);
	    	console.log('activeYear: ' + activeYear);

			app.company = new Company(activeGroup);

			app.crops = new Crops();
			app.crops.fetch({
				success: function() {

					app.fields = new Fields();
					app.fields.fetch({
						success: function() {
							
							// fetch all seedings at program startup
							// and then add newly-created ones
							app.seedings = new Seedings();
							app.seedings.fetch({
								success: function() {
									
									// vegetation on-demand
									app.vegetations = new Vegetations();
									app.start({});
								}
					    	});
						}
			    	});
				}
	    	});
	    }
    });
});
