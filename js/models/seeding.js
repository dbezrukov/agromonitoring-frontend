define([
    'underscore',
    'backbone',
], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: '_id',
        urlRoot: '/seedings',

        url: function() {
  			return this.urlRoot + '/' + this.get('field') + '/' + this.get('year');
		},

        defaults: function() {
            return {
            	// field
            	// year
	    		variety: '',
			    yield: '', 
			    tillage: '',
			    dateSow: '',
			    dateHarvest: ''
            }
        },

        initialize: function(){
            var self = this;
        },

        saveOptions: {
    		type: 'POST',
    		wait: true,
    		success: function() {
    			console.log('seeding saved');
			},
			error: function() { 
				alert('Не удается сохранить данные');
			}
        }
    });
});
