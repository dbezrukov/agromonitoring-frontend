define([
    'underscore',
    'backbone',
], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: '_id',
        urlRoot: '/machines',

        defaults: function() {
            return {
            }
        },

        initialize: function(){
            var self = this;
        },

        // get track
    });
});
