define([
    'underscore',
    'backbone',
], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: '_id',
        urlRoot: '/vegetation',

        url: function() {
  			return this.urlRoot + '/' + this.get('field') + '/' + this.get('year');
		},

        defaults: function() {
            return {
            	// field
            	// year
			    data: []
            }
        },

        initialize: function(){
            var self = this;
        }
    });
});
