define([
    'underscore',
    'backbone',
], function(_, Backbone) {

    return Backbone.Model.extend({
        urlRoot: '/news',

        defaults: function() {
            return {
            	date: '',
				title: '',
				text: '',
				href: ''
            }
        },

        initialize: function(){
            var self = this;
        }
    });
});
