define([
    'underscore',
    'backbone',
], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: '_id',
        urlRoot: '/crops',

        defaults: function() {
            return {
            	name: '',
				color: ''
            }
        },

        initialize: function(){
            var self = this;
        }
    });
});
