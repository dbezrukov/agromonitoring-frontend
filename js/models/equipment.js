define([
    'underscore',
    'backbone',
], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: '_id',
        urlRoot: '/equipment',

        defaults: function() {
            return {
            }
        },

        initialize: function(){
            var self = this;
        }
    });
});
