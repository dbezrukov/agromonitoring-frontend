define([
    'underscore',
    'backbone',
], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: '_id',
        url: '/company',

        defaults: function() {
            return {
            }
        },

        initialize: function(){
            var self = this;
        }
    });
});
