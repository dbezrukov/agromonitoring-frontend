define([
    'underscore',
    'backbone',
], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: '_id',
        urlRoot: '/fields',

        defaults: function() {
            return {
            	name: '',
				area: {
					official: 0,
					cultivated: 0,
					estimated: 0
				},
			    place: '',
			    polygon: { type: 'Polygon', coordinates: [] }
            }
        },

        initialize: function(){
            var self = this;
        }, 

        saveOptions: {
    		type: 'POST',
    		patch: true,
    		wait: true,
    		success: function() {
			},
			error: function() { 
				alert('Не удается сохранить данные');
			}
        }
    });
});
