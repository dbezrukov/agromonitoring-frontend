define([
    'underscore',
    'backbone',
], function(_, Backbone) {

    return Backbone.Model.extend({
        idAttribute: '_id',

        defaults: function() {
            return {
            	'cropId': '',
            	'area': '',
            	'yield_productivity_estimated' : '',
      			'yield_estimated' : '',
      			'yield_average' : '',
      			'yield' : ''
            }
        },

        initialize: function(){
            var self = this;
        }
    });
});
