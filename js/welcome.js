require.config({
    waitSeconds: 30,
    baseUrl: './js/',

    paths: {
        'jquery': 'bower_components/jquery/jquery.min',
        'bootstrap': 'bower_components/bootstrap/dist/js/bootstrap.min',
        'underscore': 'bower_components/underscore/underscore-min',
        'backbone': 'bower_components/backbone/backbone-min',
        'backbone.wreqr': 'bower_components/backbone.wreqr/lib/amd/backbone.wreqr.min',
        'backbone.babysitter': 'bower_components/backbone.babysitter/lib/amd/backbone.babysitter.min',
        'marionette': 'bower_components/marionette/lib/core/amd/backbone.marionette.min',
        'tpl': 'bower_components/requirejs-tpl/tpl',
    },
    shim: {
        'jquery': {
            exports: '$'
        },
        'bootstrap': {
            deps: ['jquery'],
        },
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'marionette': {
            deps: ['backbone', 'underscore', 'jquery'],
            exports: "Marionette"
        },
        'underscore': {
            exports: '_'
        }
    },
    map: {
        '*': {
            'css': 'bower_components/require-css/css.min'
        }
    }
});

require([

    'jquery',
    'underscore',
    'marionette',
    'views/header_pane',
    'views/welcome_pane',
    'models/account',
    'css!bower_components/bootstrap/dist/css/bootstrap.min.css',
 ], function($, _, Marionette, 
	HeaderPane, WelcomePane, Account) {

    var Layout = Backbone.Marionette.Layout.extend({
  		template: '#layout-template',
  		regions: {
    		header: '.header',
    		content: '.content'
  		}
	});

	// setup app and layout
	var app = new Marionette.Application;
	app.layout = new Layout();
	window.app = app;

    app.on("initialize:before", function() {
    });

    app.on("initialize:after", function() {

	  	$('body').append(app.layout.render().el);
	  	
	  	var modes = [
	  	]

	  	var menu = [
	  	]

	  	app.layout.header.show(new HeaderPane({
	  		model: app.account,
	  		modes: modes,
	  		menu: menu,
	  		color: 'rgb(142,198,63)'
	  	}));

	  	app.layout.content.show(new WelcomePane());
    });

    $(function() {

        app.account = new Account();
		
		app.account.fetch({
			success: function(){
				app.start({})

			}
	    });
    });
    
});
