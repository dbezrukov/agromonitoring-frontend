define([

    'jquery',
    'underscore',
    'sprintfjs',
    'marionette',
    'views/header_pane',
    'views/monitoring_pane',
    'views/field_pane',
    'views/fields_pane',
    'views/fields_rotation_pane',
    'views/report_yield_pane',
    'views/report_vegetation_pane',
    'views/telematics_machines_pane',
    'views/telematics_equipment_pane',
    'views/company_pane',
    'views/map_pane',

    'collections/machines',
    'collections/equipment'

], function($, _, sprintfjs, Marionette, 
	HeaderPane, MonitoringPane, 
	FieldPane, FieldsPane, FieldsRotationPane, 
	ReportYieldPane, ReportVegetationPane,
	TelematicsMachinesPane, TelematicsEquipmentPane, 
	CompanyPane, 
	MapPane,
	Machines,
	Equipment) {

	var ModalRegion = Marionette.Region.extend({
        el: ".modal",

        constructor: function() {
            Marionette.Region.prototype.constructor.apply(this, arguments);
     
            this.ensureEl();
            this.$el.on('hidden', {region:this}, function(event) {
                event.data.region.close();
            });
        },
     
        onShow: function(view) {
            view.on("close", this.onClose, this);
            this.$el.modal('show');
        },
     
        onClose: function() {
            this.$el.modal('hide');
        }
    });

	var Layout = Backbone.Marionette.Layout.extend({
  		template: '#layout-template',
  		regions: {
    		header: '.header',
    		content: '.content',
    		modal: ModalRegion
  		}
	});

	// setup app and layout
	var app = new Marionette.Application;
	app.layout = new Layout();
	window.app = app;

	// setup routes
	var Route = Backbone.Marionette.AppRouter.extend({
	  	routes : {
	  		''                     : 'goMonitoring',
	     	'monitoring'           : 'goMonitoring',
	     	'telematics-machines'  : 'goTelematicsMachines',
	     	'telematics-equipment' : 'goTelematicsEquipment',
			'map'                  : 'goMap',
	     	'fields'               : 'goFieldsList',
	     	'fields/:id'           : 'goField',
	     	'rotation'             : 'goFieldsRotation',
	     	'report-vegetation'    : 'goReportVegetation',
        	'report-yield'         : 'goReportYield',
        	'company'              : 'goCompany'
	  	},
	  	goMonitoring: function() {
	  		console.log('Go monitoring!');
	    	app.layout.content.show(new MonitoringPane());
	  	},
	  	goFieldsList: function() {

	    	app.layout.content.show(new FieldsPane({
	    		model: app.company,
	    		collection: app.fields,
        	}));
	  	},
	  	goField: function(fieldId) {

	  		var field = app.fields.get(fieldId);
	  		if (!field) {
	  			console.log('field not found');
	  			app.route.navigate('#fields', {trigger: true})
	  			return; // propably refetch here
	  		}

			console.log('name: ' + field.get('name'));
	  		
	    	app.layout.content.show(new FieldPane({
	    		model: field
        	}));
	  	},
	  	goFieldsRotation: function() {
	    	app.layout.content.show(new FieldsRotationPane({
	    		model: app.company,
	    		collection: app.fields,
        	}));
	  	},
	  	goTelematicsMachines: function(){
	  		app.layout.content.show(new TelematicsMachinesPane({
	  			collection: new Machines()
	  		}));
		},
	    goTelematicsEquipment: function(){
	    	app.layout.content.show(new TelematicsEquipmentPane({
	    		collection: new Equipment()
	    	}));
		},
		goMap: function(){
	    	app.layout.content.show(new MapPane());
		},
	    goReportVegetation: function(){
	    	app.layout.content.show(new ReportVegetationPane());
		},
        goReportYield: function(){
        	app.layout.content.show(new ReportYieldPane());
		},
		goCompany: function(){
        	app.layout.content.show(new CompanyPane({
        		model: app.company
        	}));
		}
	});

    app.on("initialize:before", function() {
    });

    app.on("initialize:after", function() {

	  	$('body').append(app.layout.render().el);
	  	
	  	var modes = [
	  		'<li><a href="#monitoring">Мониторинг</a></li>',
			'<li class="dropdown">\
          		<a class="dropdown-toggle" data-toggle="dropdown" href="#telematics">Телематика<span class="caret"></span></a>\
      			<ul class="dropdown-menu">\
        			<li><a href="#telematics-machines">Машины</a></li>\
        			<li><a href="#telematics-equipment">Оборудование</a></li>\
      			</ul>\
			</li>',
			'<li><a href="#map">Карта</a></li>',
			'<li class="dropdown">\
          		<a class="dropdown-toggle" data-toggle="dropdown" href="#fields">Поля<span class="caret"></span></a>\
      			<ul class="dropdown-menu">\
        			<li><a href="#fields">Список полей</a></li>\
        			<li><a href="#rotation">Севооборот</a></li>\
      			</ul>\
			</li>',
			'<li class="dropdown">\
          		<a class="dropdown-toggle" data-toggle="dropdown" href="#report">Отчеты<span class="caret"></span></a>\
      			<ul class="dropdown-menu">\
        			<li><a href="#report-vegetation">Отчеты о вегетации</a></li>\
        			<li><a href="#report-yield">Отчет о площади и урожайности</a></li>\
      			</ul>\
			</li>'
	  	]

		var widgets = [];

		// company select widget
		{
			var htmlTemplate = 
				'<li class="dropdown">\
			        <a class="dropdown-toggle" data-toggle="dropdown" href="#">\
			        	%s\
			        	<span class="caret"></span>\
			        </a>\
			        <ul class="dropdown-menu">\
			        	%s\
			        </ul>\
		      	</li>';

		    var menu = '';

		    for (year = 2010; year <= 2020; year++) {
				menu += '<li><a class="group-year-link" style="cursor: pointer" data-year="' + year + '">' + year + '</a></li>';
		    }

	    	var activeYear = app.account.get('addInfo').activeYear;

	    	var yearsWidget = sprintfjs.sprintf(htmlTemplate, activeYear, menu);

			widgets.push(yearsWidget);
		}

		// company select widget
		var groups = app.account.get('groups');
		if (groups.length > 0) {
			var htmlTemplate = 
				'<li class="dropdown">\
			        <a class="dropdown-toggle" data-toggle="dropdown" href="#">\
			        	<span class="glyphicon glyphicon-grain"></span>\
			        	%s\
			        	<span class="caret"></span>\
			        </a>\
			        <ul class="dropdown-menu">\
			        	%s\
			        </ul>\
		      	</li>';

		    var menu = '';

		    groups.forEach(function(group) {
		    	menu += '<li><a class="group-year-link" style="cursor: pointer" data-group="' + group._id + '">' + group.name + '</a></li>';
		    })

        	var activeGroup = _.findWhere(groups, { _id: app.account.get('addInfo').activeGroup });

        	if (activeGroup) {
        		var groupsWidget = sprintfjs.sprintf(htmlTemplate, activeGroup.name, menu);
				widgets.push(groupsWidget);
        	}
		}

	  	var menu = [
	  		'<li><a href="/auth/logout"><span class="glyphicon glyphicon-log-out"></span> Выйти</a></li>'
	  	]

	  	if (app.account.get('isAdmin')) {
			menu.unshift('<li><a href="/admin"><span class="glyphicon glyphicon-cog"></span> Администрирование</a></li>');
	  	}

	  	app.layout.header.show(new HeaderPane({
	  		model: app.account,
	  		modes: modes,
	  		widgets: widgets,
	  		menu: menu,
	  		color: 'rgb(142,198,63)'
	  	}));

	  	app.route = new Route();
	  	Backbone.history.start();

	  	// handle group or year changes
	  	$(".group-year-link").click(app.onGroupOrYearChanged);
    });

    app.onGroupOrYearChanged = function(e) {
		var groupId = $(e.target).attr('data-group');
		var year = $(e.target).attr('data-year');

		var addInfo = app.account.get('addInfo');
		
		addInfo.activeGroup = groupId || addInfo.activeGroup;
		addInfo.activeYear = year || addInfo.activeYear;

    	var options = {
			type: 'POST',
			wait: true,
			url: '/users/' + app.account.get('id'),
			success: function() {
				location.reload();
			},
			error: function() { 
				alert('Не удается сохранить данные');
			}
		}

		app.account.save({
			addInfo: addInfo
		}, options);
	};

    app.navigate = function(route, options) {
  		options || (options = {});
  		Backbone.history.navigate(route, options);
  		if (!options.trigger) {
    		return app.trackPage();
  		}
	};

	Backbone.history.on('route', function(router, route, params) {
  		return app.trackPage();
	});

	app.trackPage = function() {
		var page;
		// Google Analytics
		//if (typeof ga !== "undefined" && ga !== null) {
			page = Backbone.history.root + Backbone.history.fragment;
		//	return ga('send', 'pageview', page);
		//}

		console.log('Tracking page: ' + page);

		app.vent.trigger("page:change", page);
	};

    return app;
});
